<?php
require_once 'core/common.php';

$gameTable = '7roulette';
$resultTable = 'game_7roulette_result';

$drawTime = 10;  // n초 이상 지나면 무효처리 한다. ex: 04:30:00에 결과를 처리해야되는데 04:30:10 이후 시행될경우 무효처리

$sth = $conn->query("select * from `game_config` where `game_table`='{$gameTable}' ");
$gameConfig = $sth->fetch(PDO::FETCH_ASSOC);

$sth = $conn->query("select * from `{$resultTable}` order by `game_id` desc limit 1");
$lastGame = $sth->fetch(PDO::FETCH_ASSOC);

if($lastGame) {
    $lastGame['game_datetime_number'] = strtotime($lastGame['game_datetime']);
}
else {
    $lastGame['game_datetime_number'] = time() - ($gameConfig['game_interval'] * 60);
}

$resultMark = array(
'cherry',
'cherry',
'watermelon',
'bell',
'orange',
'grape',
'cherry',
'bell',
'777',
'cherry',
'orange',
'grape',
'cherry',
'cherry',
'orange',
'watermelon',
'cherry',
'orange',
'cherry',
'cherry',
'grape',
'orange',
'cherry',
'orange'
);

$resultColor = array(
'yellow',
'skyblue'
);

//결과를 입력해줘야할게 있다면 결과입력해준다.
$resultInsertTime = date("Y-m-d H:i:s", SERVER_TIME - (60 * $gameConfig['game_interval']));
$sql = "select * from `{$resultTable}` where `game_status` = '대기' and `game_datetime` <= '{$resultInsertTime}' order by `game_id` desc";
$res = $conn->query($sql);

foreach ($res as $row) {

    if( strtotime($resultInsertTime) - strtotime($row['game_datetime']) > $drawTime ) {
        //무효처리
        $conn->query("update `{$resultTable}` set `game_status` = '무효' where `game_id` = '{$row['game_id']}'");
    }
    else{
        //결과입력
        if($gameConfig['expect_result']) {
            $result_ball = $gameConfig['expect_result'];
        }
        else {
            $result_ball = rand(0, 23);
        }

        $color = ($result_ball % 2);
        $result_mark = $resultMark[$result_ball];
        $result_color = $resultColor[$color];

        $randomFloat = rand(1, 9) / 10;
        $rate =  (23 - $result_ball + $randomFloat) * 15;

        $conn->query("update `game_config` set `expect_result` = '' where `game_table`='{$gameTable}' ");

        $conn->query("update `{$resultTable}` set `game_status` = '완료', `result_mark` = '{$result_mark}', `result_color` = '{$result_color}', `result_ball` = '{$result_ball}', `rate` = '{$rate}' where `game_id` = '{$row['game_id']}'");
    }
}

//대기게임이 없다면 추가해준다.
$sth = $conn->query("select count(*) as `cnt` from `{$resultTable}` where `game_status` = '대기'");
$check = $sth->fetch(PDO::FETCH_ASSOC);

if($check['cnt'] == 0) {

    $game_datetime = getNextGameDatetime();
    $game_no = getNoFromDatetime($game_datetime);
    $game_status ='대기';
    $conn->query("insert `{$resultTable}` set `game_datetime` = '{$game_datetime}', `game_no` = '{$game_no}', `game_status` = '{$game_status}'");
}

function getNextGameDatetime() {
    global $gameConfig;

    $tmp_time = time() + 30; //30초정도 여유

    $datetime = $tmp_time - ($tmp_time % (60 * $gameConfig['game_interval']));

    return date("Y-m-d H:i:s", $datetime);
}

function getNoFromDatetime($datetime) {
    global $gameConfig;

    $time_gap = strtotime($datetime) - strtotime(TIME_YMD);
    return ($time_gap / (60 * $gameConfig['game_interval'])) + 1;
}