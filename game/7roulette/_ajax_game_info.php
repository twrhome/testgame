<?php
require_once 'core/common.php';
include_once('./game.config.php');

//$time_gap = 46814;

$game_interval = $gameConfig['game_interval'];
$no_length = 4;

function get_next_info() {
    global $game_interval, $no_length;
    $time_now = time();
    $total_time = 60 * $game_interval;
    $pass_time = $time_now % $total_time;

    $next_time = $time_now + ($total_time - $pass_time);
    $next_date = date("Y-m-d", $next_time);
    $next_no = (($next_time - strtotime($next_date)) / ($total_time));
    $next_datetime_no = str_replace("-","",$next_date) . substr("0000" . $next_no, $no_length * (-1));
    $return_array = array("next_no" => $next_no, "time" => $next_time, "datetime_no" => $next_datetime_no);
    return $return_array;
}

$next_info = get_next_info();

$sth = $conn->query("select * from `{$resultTable}` where `game_status` = '완료' order by `game_id` desc limit 1");
$last_info = $sth->fetch(PDO::FETCH_ASSOC);

$sth = $conn->query("SELECT IFNULL(MAX(cnt1), 0) AS `cherry`,  IFNULL(MAX(cnt2), 0) AS `orange`,  IFNULL(MAX(cnt3), 0) AS `grape`,  IFNULL(MAX(cnt4), 0) AS `watermelon`,  IFNULL(MAX(cnt5), 0) AS `bell`,  IFNULL(MAX(cnt6), 0) AS `777` FROM (
  SELECT

    CASE WHEN `result_mark`='cherry' THEN @c1:=@c1+1 END AS cnt1,
    CASE WHEN `result_mark`='orange' THEN @c2:=@c2+1 END AS cnt2,
    CASE WHEN `result_mark`='grape' THEN @c3:=@c3+1 END AS cnt3,
    CASE WHEN `result_mark`='watermelon' THEN @c4:=@c4+1 END AS cnt4,
    CASE WHEN `result_mark`='bell' THEN @c5:=@c5+1 END AS cnt5,
    CASE WHEN `result_mark`='777' THEN @c6:=@c6+1 END AS cnt6

  FROM `{$resultTable}`, (SELECT @c1:=0,@c2:=0,@c3:=0,@c4:=0,@c5:=0,@c6:=0) AS `t1`
  WHERE `result_mark` != '' order by `game_id` desc limit 100
) AS `t2`");

$static = $sth->fetch(PDO::FETCH_ASSOC);

function mark_state_table() {

  global $conn, $resultTable;

  $sth = $conn->query("select * from (select * from `{$resultTable}` where `result_mark` != '' order by `game_id` desc limit 100) as a order by `game_id` asc");

  $pattern_type = 'result_mark';
  $ex_pattern = '';
  $pattern_array = array();
  $pattern_now = 0;
  $max_row = 0;
  $tmp_row = 0;

  $col_count = array();
  while($row = $sth->fetch(PDO::FETCH_ASSOC)){
    if($row[$pattern_type] != $ex_pattern){
      array_push($col_count,0);

      //상단 메뉴기때문에 컬러를 조금 입혀주자
      $tmp_text = $row[$pattern_type];
      /*
      if($pattern_type == 'result_mark' && $tmp_text == 'GOLD'){
        $tmp_text = "<td class='state_gold'>금화</td>";
      }
      else if($pattern_type == 'result_mark' && $tmp_text == 'TREASURE'){
        $tmp_text = "<td class='state_treasure'>보물</td>";
      }
      else if($pattern_type == 'result_mark' && $tmp_text == 'TIGER'){
        $tmp_text = "<td class='state_tiger'>호랑이</td>";
      }
      else if($pattern_type == 'result_mark' && $tmp_text == 'BAMBOO'){
        $tmp_text = "<td class='state_bamboo'>대나무</td>";
      }
      else if($pattern_type == 'result_mark' && $tmp_text == 'PANDA'){
        $tmp_text = "<td class='state_panda'>판다</td>";
      }
      */
      //끝
      array_push($pattern_array, array($tmp_text));

      $pattern_now = count($pattern_array) -1;
      $tmp_row = 1;
    }
    else{
      ++$tmp_row;
    }

    ++$col_count[(count($col_count)-1)];
    if($tmp_row > $max_row)
      $max_row = $tmp_row;

    /*
    //둥근 원을 그릴 수 있도록 css를 입혀보자
    $tmp_class = 'state_';
    if($row['result_color'] == 'yellow'){
      $tmp_class .= '_yellow';
    }
    else if($row['panda_result'] == 'skyblue'){
      $tmp_class .= '_skyblue';
    }
    */

    $tmp_text = "<td><img src=\"./img/pd_topbox_icon_" . strtolower($row['result_mark']) . ".png\"></td>";
    //끝

    array_push($pattern_array[$pattern_now], $tmp_text);
    $ex_pattern = $row[$pattern_type];
  }

  $max_col = count($pattern_array);
  $html_pattern = '';
  for($i=0; $i < $max_row+1 ;$i++){
    $html_pattern .= '<tr>';
    for($j=0; $j < $max_col ;$j++){
      if($pattern_array[$j][$i]){
        $html_pattern .= $pattern_array[$j][$i];
      }
      else{
        $html_pattern .= "<td></td>";
      }
    }
    $html_pattern .= '</tr>';
  }

  return $html_pattern;
}

function color_state_table() {

  global $conn, $resultTable;

  $sth = $conn->query("select * from (select * from `{$resultTable}` where `result_color` != '' order by `game_id` desc limit 100) as a order by `game_id` asc");

  $pattern_type = 'result_color';
  $ex_pattern = '';
  $pattern_array = array();
  $pattern_now = 0;
  $max_row = 0;
  $tmp_row = 0;

  $col_count = array();
  while($row = $sth->fetch(PDO::FETCH_ASSOC)){
    if($row[$pattern_type] != $ex_pattern){
      array_push($col_count,0);

      //상단 메뉴기때문에 컬러를 조금 입혀주자
      $tmp_text = $row[$pattern_type];
      /*
      if($pattern_type == 'result_color' && $tmp_text == 'yellow'){
        $tmp_text = "<td class='state_yellow'>yellow</td>";
      }
      else if($pattern_type == 'result_color' && $tmp_text == 'skyblue'){
        $tmp_text = "<td class='state_skyblue'>skyblue</td>";
      }
      */
      //끝

      array_push($pattern_array, array($tmp_text));

      $pattern_now = count($pattern_array) -1;
      $tmp_row = 1;
    }
    else{
      ++$tmp_row;
    }
    ++$col_count[(count($col_count)-1)];
    if($tmp_row > $max_row)
      $max_row = $tmp_row;

    $tmp_text = "<td><img src=\"./img/pd_topbox_icon_" . strtolower($row['result_color']) . ".png\"></td>";
    //끝

    array_push($pattern_array[$pattern_now], $tmp_text);
    $ex_pattern = $row[$pattern_type];
  }

  $max_col = count($pattern_array);
  $html_pattern = '';
  for($i=0; $i < $max_row+1 ;$i++){
    $html_pattern .= '<tr>';
    for($j=0; $j < $max_col ;$j++){
      if($pattern_array[$j][$i]){
        $html_pattern .= $pattern_array[$j][$i];
      }
      else{
        $html_pattern .= "<td></td>";
      }
    }
    $html_pattern .= '</tr>';
  }

  return $html_pattern;
}

$result = array();
$result['last_info'] = $last_info;
$result['next_info'] = $next_info;
$result['static'] = $static;
$result['mark_state_table'] = mark_state_table();
$result['color_state_table'] = color_state_table();

echo json_encode($result);
?>

