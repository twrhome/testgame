<?php
//require_once '../../lib/core/common.php';
require_once 'core/common.php';
include_once('./game.config.php');

?>

<!DOCTYPE HTML>
<head>
	<meta charset="utf-8">
	<title>세븐룰렛</title>
  <link rel='stylesheet' type='text/css' href='./style.css?t=<?php echo time();?>'>
	<link rel="stylesheet" href="./js/jquery.mCustomScrollbar.css" />
  <script type="text/javascript" src="./js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="./js/jquery.mCustomScrollbar.min.js"></script>
  <script type="text/javascript" src="./js/jquery.easing.js"></script>
  <script type='text/javascript' src='./js/jQueryRotate.js'></script>
</head>
<body>

<div id="sub_wrap">
  <div class="pd_pan">
    <div class="pd_topbox">
      <!--<div class="pd_topbox_time"><img src="./img/time_icon.png" style="width:auto;height:16px;margin-top:-3px;margin-right:8px;">00:02:05</div>-->
      <div class="pd_topbox_left_result">
        <h3>최신결과<p></p></h3><div class="result_icon"></div>
      </div>
      <div class="pd_topbox_right_result">
        <span>최신 100회 통계</span>
        <div class="pd_topbox_right_result_btn pd_btn">추첨통계</div>
        <!---->
          <div class="pd_result_list">
            <div class="pd_result_list_top">
              <div rel="pattern-scroll-box1" class="pd_result_list_top_btn selected">그림</div>
              <div rel="pattern-scroll-box2" class="pd_result_list_top_btn">컬러볼</div>
              <div onClick="state_box_open();" style="position:absolute;top:0;right:0;background:url('./img/menu_x02.png') center center;background-size:100%;width:30px;height:30px;cursor:pointer;"></div>
            </div>
            <div class="pd_result_list_area">
            <table class="pd_result_list_table" id="pattern-scroll-box1">
            </table>
            <table class="pd_result_list_table" id="pattern-scroll-box2">
            </table>
            </div>
          </div>
        <!---->
        <div class="pd_topbox_right_sound_btn" id="btn_sound"><img src="./img/pd_topbox_sound_off.png" class="sound_img"></div>
        <div class="pd_topbox_right_result_count">
          <div class="pd_count pd_count_cherry" style="margin-left:0;">0</div>
          <div class="pd_count pd_count_orange">0</div>
          <div class="pd_count pd_count_grape">0</div>
          <div class="pd_count pd_count_watermelon">0</div>
          <div class="pd_count pd_count_bell">0</div>
          <div class="pd_count pd_count_777">0</div>
        </div>
        <!--pd_topbox_right_result_count-->
      </div>
      <!--pd_topbox_right_result-->
    </div>
    <!--pd_topbox-->
    <!--
    <div class="pd_sound_box">
      <div class="pd_sound_btn"><img src="./img/pd_b_sound_on.png" class="sound_img"></div>
      <div class="reset_btn" onClick="window.location.reload()"><img src="./img/pd_b_reset.png" class="reset_img"></div>
    </div>
    -->
    <div class="pd_game_box">
      <div class="pd_game_light01"></div><div class="pd_game_light02"></div><div class="pd_game_light03"></div>
      <div class="pd_game_pan"></div>
      <div class="pd_game_roulette"></div>
      <div class="pd_game_pin"></div>

      <div class="pd_text_box"><h2></h2><p></p></div>
    </div>
    <!--pd_game_box-->
    <div class="pd_vs_box">
      <div class="pd_vs_box_top">
        <div class="graph_yellow_area"><span id="graph_yellow" style="width:0%"></span></div>
        <strong class="vs_area"><span id="txt_yellow_per" class="txt_per">0.00%</span><em class="ico_vs">VS</em><span id="txt_skyblue_per" class="txt_per">0.00%</span></strong>
        <div class="graph_skyblue_area"><span id="graph_skyblue" style="width:0%"></span></div>
      </div>
      <div class="pd_vs_box_bottom">구매분포는 구매인원에 따라 그래프가 변동되며, 추첨과는 무관합니다</div>

      <!--div class="game_vs_bar">
        <div class="this_times_chart">
          <div class="graph_yellow_area"><span id="graph_yellow" style="width:0%"></span></div>
          <strong class="vs_area"><span id="txt_yellow_per" class="txt_per">0%</span><em class="ico_vs">VS</em><span id="txt_skyblue_per" class="txt_per">0%</span></strong>
          <div class="graph_skyblue_area"><span id="graph_skyblue" style="width:0%"></span></div>
        </div>
      </div-->
    </div>
  </div>
</div>	<!-- sub_wrap -->
<div id="bgm">
    <audio id="bgmPlayer">
        <source src="./sound/start.mp3" type="audio/mp3">
    </audio>
    <audio id="resultPlayer">
        <source src="./sound/result.mp3" type="audio/mp3">
    </audio>
</div>

<script type="text/javascript">
var next_time = 9999;
var did_sound = false;
var did_request_result = false;
var did_clear_per = false;
var did_success = false;
var last_game_id = 0;
var last_game;
var next_game;
var load_done = 0;
var state_box_state = 0;
var isRotated = true;
var BgmPlayer = document.getElementById('bgmPlayer');
var ResultPlayer = document.getElementById('resultPlayer');

BgmPlayer.loop = true;
ResultPlayer.loop = true;

$(document).ready(function() {

    get_next_match();

    setInterval(function(){
        if(load_done){
            check_time();
        }
    }, 1000);

    setInterval(function(){
        get_graph();
    },5000);

    $('#btn_sound').click(function(e) {
        e.preventDefault();
        var $self = $(this);

        //$self.toggleClass("on");

        if(did_sound == false) {
            did_sound = true;
            $self.html('<img src="./img/pd_topbox_sound_on.png">');
            BgmPlayer.play();
        } else {
            did_sound = false;
            $self.html('<img src="./img/pd_topbox_sound_off.png">');
            BgmPlayer.pause();
        }
    });

    $('.pd_btn').click(function(e) {
        e.preventDefault();
        var $self = $(this);
        state_box_open();
    });

    $(".pd_result_list_table").hide();
    $(".pd_result_list_table:first").show();

    $(".pd_result_list_top_btn").click(function () {
        $(".pd_result_list_top_btn").removeClass("selected");
        $(this).addClass("selected");
        $(".pd_result_list_table").hide()
        var activeTab = $(this).attr("rel");
        $("#" + activeTab).show();
    });

});

function get_next_match(){

    $.ajax({
        type: 'post',
        url:'./_ajax_game_info.php',
        dataType:'json',
        success:function(result){

            if(isRotated) {
                $(".pd_game_pan").rotate({
                    angle: parseFloat(result['last_info']['rate']),
                    center: ["50%", "50%"],
                    easing: $.easing.easeOutQuart,
                    callback: function(){
                        isRotated = false;
                    },
                    duration:0
                });
            }

            next_time = result['next_info']['time'];
            last_game_id = result['last_info']['game_id'];

            $(".pd_topbox_left_result p").html(result['last_info']['game_no'] + "회차");
            $(".pd_text_box h2").html(result['next_info']['next_no'] + "회차");

            $(".result_icon").html("<img src=\"./img/pd_topbox_" + result['last_info']['result_mark'] + "_" +  result['last_info']['result_color'] + ".png\">");
            //$("#bar_center").html(result['next_info']['no']+"회차 구매분포");
            //$("#result_list_box").html(result['result_list_info']);
            $(".pd_count_cherry").html(result['static']['cherry']);
            $(".pd_count_orange").html(result['static']['orange']);
            $(".pd_count_grape").html(result['static']['grape']);
            $(".pd_count_watermelon").html(result['static']['watermelon']);
            $(".pd_count_bell").html(result['static']['bell']);
            $(".pd_count_777").html(result['static']['777']);

            $("#pattern-scroll-box1").html(result['mark_state_table']);
            $("#pattern-scroll-box2").html(result['color_state_table']);

            //$('#pattern-scroll-box1').scrollLeft($('#pattern-scroll-box1').outerWidth());
            //$('#pattern-scroll-box2').scrollLeft($('#pattern-scroll-box2').outerWidth());

            last_game = result['last_info'];
            next_game = result['next_info'];
            load_done = 1;
            did_request_result = false;

            //window.parent.get_bet_list();

        }//success
    });//ajax

}

function state_box_open(){
	if(state_box_state == 0){
		$('.pd_result_list').show();
		state_box_state = 1;
	} else {
		$('.pd_result_list').hide();
		state_box_state = 0;
	}
}

function check_time(){

    $.ajax({
        type: 'GET',
        url: '_ajax_time.php',
        success: function(st) {
            var this_time = st;
            var remain_time = next_time - this_time;
            var remain_min = ("00" + ((remain_time - (remain_time % 60)) / 60)).slice(-2);
            var remain_sec = ("00" + (remain_time - (remain_min * 60))).slice(-2);

            if(remain_time >= 0){
                $(".pd_text_box p").html("00:"+remain_min+":"+remain_sec);
            }
            else if(remain_time > -2 && did_request_result == false){

                $(".pd_text_box p").html("추첨 준비중");

                //$(".pd_game_pan").addClass("ani");
                //$(".pd_game_pin").addClass("ani");
                //$(".pd_game_light01").addClass("ani");
                //$(".pd_game_light02").addClass("ani");
                //$(".pd_game_light03").addClass("ani");
            }
            else if(remain_time < -2 && did_request_result == false){

                get_result_info();
            }

            if(did_success === false && remain_time < -50){  // 이것은 궁여지책이다.
                location.reload();
            }

            if(remain_time <= 5 && did_clear_per == false){
                did_clear_per = true;
                $("#graph_yellow").animate({"width":"0%"}, 300);
                $("#graph_skyblue").animate({"width":"0%"}, 300);
                $("#txt_yellow_per").html("0%");
                $("#txt_skyblue_per").html("0%");
            }
        }
    });
}

function get_result_info(){

    did_request_result = true;
    $.ajax({
        type: 'post',
        url:'./_ajax_result.php',
        dataType:'json',
        data:{last_id : last_game_id},
        success:function(result){
            if(result['success'] == true){
                did_success = true;
                go_animate(result);
            }
            else{
                setTimeout(function(){
                    get_result_info();
                }, 1000);
            }
        }
    });
}

function go_animate(result){

  if(did_sound == true) {
      BgmPlayer.pause();
      ResultPlayer.play();
  }

  $(".pd_text_box").hide();

  $(".pd_game_pin").addClass("ani");
  $(".pd_game_light01").addClass("ani");
  $(".pd_game_light02").addClass("ani");
  $(".pd_game_light03").addClass("ani");

  isRotated = true;
	$(".pd_game_pan").rotate({
	  	angle:0,
	  	animateTo: 360 * 10 + parseFloat(result['rate']),
	  	center: ["50%", "50%"],
	  	easing: $.easing.easeOutQuart,
	  	callback: function(){

			  isRotated = false;
        did_clear_per = false;

        //$(".pd_game_pan").removeClass("ani");
        $(".pd_game_pin").removeClass("ani");
        $(".pd_game_light01").removeClass("ani");
        $(".pd_game_light02").removeClass("ani");
        $(".pd_game_light03").removeClass("ani");

        if(did_sound == true) {
            ResultPlayer.pause();
            BgmPlayer.play();
        }

        $(".pd_text_box h2").html("");
        $(".pd_text_box p").html("배당률 준비중");
        $(".pd_text_box").show();

        get_next_match();

		  },
		  duration:10000
	});
}

function get_graph(){

    if(did_clear_per == false){

        $.ajax({
            type: 'GET',
            url:'./_ajax_graph.php',
            dataType:'json',
            success:function(result){
                $("#graph_yellow").animate({"width":result['yellow']+"%"}, 300);
                $("#graph_skyblue").animate({"width":result['skyblue']+"%"}, 300);
                $("#txt_yellow_per").html(result['yellow']+"%");
                $("#txt_skyblue_per").html(result['skyblue']+"%");
            }//success
        });//ajax
    }
}

if(window.location.ancestorOrigins !== undefined) {

	var result = "";
	for(var i = 0; i <window.location.ancestorOrigins.length; i++){
		if(i >0) result = result + "|";
		url = window.location.ancestorOrigins[i];
		result += url.replace(/(^\w+:|^)\/\//, '');
	}

	$.ajax({
		url: '_ajax_get_access_url.php',
		type: 'GET',
		data : 'mode=get_access_url&result='+result,
		dataType: 'jsonp',
		timeout: 10000,
		success: function(data) {
			if(data.success){
        console.log(data.message);
			}else{
				console.log(data.message);
			}
		}, error: function() {

		}
	});
}
</script>
</body>
</html>