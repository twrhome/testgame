<?php
require_once 'core/common.php';
include_once('./game.config.php');

$last_id = $_POST['last_id'];

$sth = $conn->query("select * from `{$resultTable}` where `game_id` > '{$last_id}' and `game_status` = '완료' order by `game_id` desc limit 1");
$row = $sth->fetch(PDO::FETCH_ASSOC);

if($row){

  if(empty($row['result_mark'])) {
    echo json_encode(array("success" => false));
    exit;
  }

  if(empty($row['result_color'])) {
    echo json_encode(array("success" => false));
    exit;
  }

  if(empty($row['result_ball'])) {
    echo json_encode(array("success" => false));
    exit;
  }

  if(empty($row['rate'])) {
    echo json_encode(array("success" => false));
    exit;
  }

  echo json_encode(array("success" => true, "result_mark" => $row['result_mark'], "result_color" => $row['result_color'], "rate" => $row['rate'], "game_no" => $row['game_no']));
}
else{
  echo json_encode(array("success" => false, "last_id" => $last_id));
}
?>