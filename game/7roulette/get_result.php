<?php
header('Access-Control-Allow-Origin: *');
include_once(dirname(__FILE__) . "/../../common.php");


$table_name = "es_luckypanda";

$json_data = stripslashes($_POST['json_data']);

$result = json_decode($json_data, true);

//{"game_id":"28602181","num":"201903060044","result":{"mark":"MARK:GOLD","color":"COLOR:PEACH","ball":2},"state":3}
foreach($result as $key1 => $val1){

    $id = $val1['game_id'];
    $datetime_no = $val1['num'];
    $datetime = substr($val1['num'],0,4) . '-' . substr($val1['num'],4,2) . '-' . substr($val1['num'],6,2);
    $no = (int)substr($val1['num'],8);
    $state = $val1['state'];

    $result_mark = str_replace('MARK:', '', $val1['result']['mark']);
    $result_color = str_replace('COLOR:', '', $val1['result']['color']);
    $result_ball = $val1['result']['ball'];

    $check_duplicate = sql_fetch("select count(*) as `cnt` from `{$table_name}` where `id` = '{$id}'");

    if($check_duplicate['cnt'] > 0){

        sql_query("update `{$table_name}` set `datetime_no` = '{$datetime_no}', `datetime` = '{$datetime}', `no` = '{$no}', `result_mark` = '{$result_mark}', `result_color` = '{$result_color}', `result_ball` = '{$result_ball}', `state` = '{$state}' where `id` = '{$id}'");

        break; // 혹시 과부하 될까봐 넣어둠.
    }
    else {

        sql_query("insert `{$table_name}` set `datetime_no` = '{$datetime_no}', `id` = '{$id}',  `datetime` = '{$datetime}', `no` = '{$no}', `result_mark` = '{$result_mark}', `result_color` = '{$result_color}', `result_ball` = '{$result_ball}' , `graph_peach` = '0', `graph_purple` = '0', `state` = '{$state}'");
    }
}
?>