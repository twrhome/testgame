-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- 생성 시간: 21-06-07 15:17
-- 서버 버전: 10.1.34-MariaDB
-- PHP 버전: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 데이터베이스: `gamezone`
--

-- --------------------------------------------------------

--
-- 테이블 구조 `game_config`
--

CREATE TABLE `game_config` (
  `config_id` int(11) NOT NULL,
  `game_table` varchar(20) NOT NULL,
  `game_interval` int(11) NOT NULL,
  `betting_win_rate` decimal(4,2) NOT NULL DEFAULT '0.00',
  `betting_time_limit` int(11) NOT NULL,
  `betting_point_min` int(11) NOT NULL,
  `betting_point_max` int(11) NOT NULL,
  `expect_result` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 테이블의 덤프 데이터 `game_config`
--

INSERT INTO `game_config` (`config_id`, `game_table`, `game_interval`, `betting_win_rate`, `betting_time_limit`, `betting_point_min`, `betting_point_max`, `expect_result`) VALUES
(8, '7roulette', 1, '0.00', 0, 0, 0, '');

--
-- 덤프된 테이블의 인덱스
--

--
-- 테이블의 인덱스 `game_config`
--
ALTER TABLE `game_config`
  ADD PRIMARY KEY (`config_id`);

--
-- 덤프된 테이블의 AUTO_INCREMENT
--

--
-- 테이블의 AUTO_INCREMENT `game_config`
--
ALTER TABLE `game_config`
  MODIFY `config_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
