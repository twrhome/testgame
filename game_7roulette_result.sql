-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- 생성 시간: 21-06-07 15:18
-- 서버 버전: 10.1.34-MariaDB
-- PHP 버전: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 데이터베이스: `gamezone`
--

-- --------------------------------------------------------

--
-- 테이블 구조 `game_7roulette_result`
--

CREATE TABLE `game_7roulette_result` (
  `game_id` int(11) NOT NULL,
  `game_no` int(11) NOT NULL,
  `game_datetime` varchar(20) NOT NULL,
  `game_status` varchar(20) NOT NULL,
  `result_mark` varchar(10) NOT NULL,
  `result_color` varchar(10) NOT NULL,
  `result_ball` smallint(2) NOT NULL,
  `rate` float NOT NULL,
  `betting_rate` varchar(10) NOT NULL,
  `insert_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 덤프된 테이블의 인덱스
--

--
-- 테이블의 인덱스 `game_7roulette_result`
--
ALTER TABLE `game_7roulette_result`
  ADD PRIMARY KEY (`game_id`);

--
-- 덤프된 테이블의 AUTO_INCREMENT
--

--
-- 테이블의 AUTO_INCREMENT `game_7roulette_result`
--
ALTER TABLE `game_7roulette_result`
  MODIFY `game_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
