<?php
ini_set("display_errors", 1);
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);

define( 'BASEPATH',

  call_user_func( function( $base, $pwd ) {
    return '.' . preg_replace( '/\/[^\/]+/', '/..', str_replace( $base, '', $pwd ) );

  }, realpath( __DIR__ . '/../..' ), realpath( '.' )

));

define('SERVER_TIME',    time());
define('TIME_YMDHIS',    date('Y-m-d H:i:s', SERVER_TIME));
define('TIME_YMD',       substr(TIME_YMDHIS, 0, 10));
define('TIME_HIS',       substr(TIME_YMDHIS, 11, 8));

require_once __DIR__.'/define.php';
require_once __DIR__.'/config.php';
require_once __DIR__.'/activerecord.php';

function session_get($key)
{
  if (FALSE) {}

  return FALSE;
}

function session_set($key, $value)
{
  if (FALSE) {}

}

// http parameter - type GET
function param_get($key)
{
  if (!isset($_GET[$key]))
    return FALSE;
  return $_GET[$key];
}

// http parameter - type POST
function param_post($key)
{
  if (!isset($_POST[$key]))
    return FALSE;
  return $_POST[$key];
}

function format_date($datetime, $guard = FALSE)
{
  if (!$datetime) return $guard;

  if (is_string($datetime))
  {
    $datetime = DateTime::createFromFormat('YmdHis', $datetime.'000000');
    if (!$datetime) return $guard;
  }

  return $datetime->format('Y-m-d');
}

function format_datetime($then, $guard = FALSE)
{
  $now = server_time();
  if (!$then OR !$now)
    return $guard;

  $day_after_then = DateTime::createFromFormat('Ymd', $then->format('Ymd'));
  $day_after_then->add(new DateInterval('P1D'));

  if ($then->format('Ymd') === $now->format('Ymd'))
    return ($then->format('a') === 'am' ? '오전 ' : '오후 ').$then->format('h:i');

  else if ($day_after_then->format('Ymd') === $now->format('Ymd'))
    return $then->format('어제 H:i');

  else
    return $then->format('Y-m-d H:i');
}

function server_time()
{
  $now = DateTime::createFromFormat('U', SERVERTIME);
  $now->setTimezone(new DateTimeZone(date_default_timezone_get()));
  return $now;
}

function field_cut($str, $length)
{
  return trim(mb_substr($str, 0, $length, 'utf-8'));
}

function field_len( $str )
{
  return mb_strlen( $str, 'utf-8' );
}

function field_guard( $str, $guard = '-' )
{
  return strlen( $str ) ? $str : $guard;
}

function ellipsis( $text, $max = 100, $append = '&hellip;' )
{
  if ( strlen( $text ) <= $max ) return $text;
  $out = substr( $text, 0, $max );
  if ( strpos( $text, ' ' ) === FALSE ) return $out.$append;
  return preg_replace( '/\w+$/', '', $out ).$append;
}

// 숫자 아니면 제거
function remove_none_digits($str)
{
  return preg_replace('/[^[:digit:]]/', '', $str);
}

// 전화번호 포멧팅.
function format_tel($str)
{
  if (preg_match('/^[[:digit:]]{5,8}$/', $str) === 1)
    return preg_replace('/^([[:digit:]]{1,4})([[:digit:]]{4})$/', '\\1-\\2', $str);

  if (preg_match('/^[[:digit:]]{9}$/', $str) === 1)
    return preg_replace('/^([[:digit:]]{2})([[:digit:]]{3})([[:digit:]]{4})$/', '\\1-\\2-\\3', $str);

  if (preg_match('/^02[[:digit:]]{8}$/', $str) === 1)
    return preg_replace('/^([[:digit:]]{2})([[:digit:]]{4})([[:digit:]]{4})$/', '\\1-\\2-\\3', $str);

  if (preg_match('/^[[:digit:]]{10}$/', $str) === 1)
    return preg_replace('/^([[:digit:]]{3})([[:digit:]]{3})([[:digit:]]{4})$/', '\\1-\\2-\\3', $str);

  if (preg_match('/^[[:digit:]]{11}$/', $str) === 1)
    return preg_replace('/^([[:digit:]]{3})([[:digit:]]{4})([[:digit:]]{4})$/', '\\1-\\2-\\3', $str);
  return $str;
}

// 사업자 등록번호 포멧팅.
function format_reg_num($str)
{
  if (preg_match('/^[[:digit:]]{10}$/', $str) === 1)
    return preg_replace('/^([[:digit:]]{3})([[:digit:]]{2})([[:digit:]]{5})$/', '\\1-\\2-\\3', $str);
  return $str;
}

// input[type=date] 용 날짜 포멧: YYYY-MM-DD
function format_input_type_date($val)
{
  $date = DateTime::createFromFormat('YmdHis', $val.'000000');
  return !$date ? '' : $date->format('Y-m-d');
}

// multi-dimensional array에 사용자지정 함수적용
function array_map_deep($fn, $array)
{
    if(is_array($array)) {
        foreach($array as $key => $value) {
            if(is_array($value)) {
                $array[$key] = array_map_deep($fn, $value);
            } else {
                $array[$key] = call_user_func($fn, $value);
            }
        }
    } else {
        $array = call_user_func($fn, $array);
    }

    return $array;
}

function get_selected($field, $value)
{
    return ($field==$value) ? ' selected="selected"' : '';
}


function get_checked($field, $value)
{
    return ($field==$value) ? ' checked="checked"' : '';
}

// 파일을 업로드 함
function upload_file($srcfile, $destfile, $dir)
{
    if ($destfile == "") return false;
    // 업로드 한후 , 퍼미션을 변경함
    @move_uploaded_file($srcfile, $dir.'/'.$destfile);
    @chmod($dir.'/'.$destfile, FILE_PERMISSION);
    return true;
}

function is_mobile() {

    $mAgent = array("iPhone","iPod","Android","Blackberry", "Opera Mini", "Windows ce", "Nokia", "sony" );
    $chkMobile = false;
    for($i=0; $i<sizeof($mAgent); $i++){
        if(stripos( $_SERVER['HTTP_USER_AGENT'], $mAgent[$i] )){
            $chkMobile = true;
            break;
        }
    }

    return $chkMobile;
}

// session
session_save_path( SESSION_DIRECTORY );
session_cache_expire(36000);
session_set_cookie_params( SESSION_COOKIE_EXPIRE, SESSION_COOKIE_PATH, SESSION_COOKIE_DOMAIN);
session_start();


//Login::init( $_SESSION[ 'admin' ] );

$conn = ActiveRecord\ConnectionManager::get_connection();