<?php
require_once 'php-activerecord/ActiveRecord.php';

ActiveRecord\Config::initialize(function($cfg)
{
  $cfg->set_model_directory('/var/www/www/models');
  $cfg->set_connections(array(
    'development' => 'mysql://'.DATABASE_LOGIN.':'.DATABASE_PASSWORD.'@'.DATABASE_HOST.'/'.DATABASE_NAME.';charset='.DATABASE_CHARSET));
});

class ConditionBuilder
{
  private $separator;
  private $precondition;
  private $pairs = array();
  private $raw_conditions = array();

  function __construct($separator = 'AND', $precondition = '')
  {
    $this->separator = ' '.$separator.' ';
    $this->precondition = $precondition;
  }

  function add($condition, $value)
  {
    $this->pairs[] = array($condition, $value);
  }

  function add_raw($condition)
  {
    $this->raw_conditions[] = $condition;
  }

  function build()
  {
    $condition_strings = array_map(function($pair) { return $pair[0]; }, $this->pairs);
    $condition_values = array_map(function($pair) { return $pair[1]; }, $this->pairs);
    if (strlen($this->precondition))
    {
      $condition = array(
        $this->precondition.' AND ('
        .implode($this->separator, array_merge($condition_strings, $this->raw_conditions))
        .')'
      );
    }
    else
    {
      $condition = array(implode($this->separator, array_merge($condition_strings, $this->raw_conditions)));
    }
    foreach ($condition_values as $value)
      $condition[] = $value;
    return $condition;
  }
}

function escape_db_string_in_single_quote($str)
{
  return str_replace("'", "\\'", $str);
}

function mysql_password($password)
{
  $dbh = new PDO('mysql:dbname='.DATABASE_NAME.';host='.DATABASE_HOST, DATABASE_LOGIN, DATABASE_PASSWORD,
    array('charset' => DATABASE_CHARSET));
  $sth = $dbh->prepare("SELECT password('".$password."')");
  $sth->execute();
  $result = $sth->fetch();
  return $result[0];
}

function now()
{
  $dbh = new PDO('mysql:dbname='.DATABASE_NAME.';host='.DATABASE_HOST, DATABASE_LOGIN, DATABASE_PASSWORD,
    array('charset' => DATABASE_CHARSET));
  $sth = $dbh->prepare("SELECT now()");
  $sth->execute();
  $result = $sth->fetch();
  return $result[0];
}

// 단순 텍스트 길이 검증.
function validate_string_length($str, $min, $max)
{
  $len = mb_strlen($str, 'utf-8');
  return $len >= $min AND $len <= $max;
}
