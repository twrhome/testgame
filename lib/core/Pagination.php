<?php

class Pagination
{
  const PAGE_LINK_COUNT = 10;

  private $item_count = 0;
  private $page_size;
  public $page_total = 0;
  public $page = 0;
  public $base;
  public $last;

  function __construct($item_count, $page_size)
  {
    $this->item_count = $item_count;

    $this->page_size = intval( $page_size );
    if( intval( $page_size < 1 ) )
      $this->page_size = 30;

    $this->page_total = ceil($item_count / $this->page_size);
  }

  function page($page)
  {
    $this->page = intval( $page );
    if ($this->page === -1 OR $this->page >= $this->page_total) $this->page = $this->page_total - 1;
    if ($this->page < 0) $this->page = 0;
    $this->base = $this->page;
  }

  function base($base)
  {
    $this->base = intval( $base );
    if ($this->base === -1 OR $this->base >= $this->page_total) $this->base = $this->page_total - self::PAGE_LINK_COUNT;
    if ($this->base < 0) $this->base = 0;
    if ($this->page < $this->base) $this->base = $this->page;
    if ($this->page >= $this->base + self::PAGE_LINK_COUNT) $this->base = $this->page_total - self::PAGE_LINK_COUNT;
    $this->last = $this->base + self::PAGE_LINK_COUNT - 1 < $this->page_total
      ? $this->base + self::PAGE_LINK_COUNT - 1
      : $this->page_total - 1;
  }

  function offset()
  {
    return $this->page * $this->page_size;
  }

  function limit()
  {
    return $this->page_size;
  }

  function populate($fn)
  {
    $arr = array();
    foreach (range($this->base, $this->last) as $index)
      $arr[] = call_user_func($fn, $index, $index == $this->page);
    return $arr;
  }

  function item_count()
  {
    return $this->item_count;
  }

  function item_index( $index )
  {
    return $this->item_count() - $this->offset() - $index - 1;
  }
}
