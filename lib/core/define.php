<?php
define( 'ENV_DEVELOPMENT', 'development' );
define( 'ENV_TEST',        'test' );
define( 'ENV_RELEASE',     'release' );


define('DATA_DIR',       'data');

// 퍼미션
define('DIR_PERMISSION',  0755); // 디렉토리 생성시 퍼미션
define('FILE_PERMISSION', 0644); // 파일 생성시 퍼미션

define( 'WEBPATH', BASEPATH.'/web' );
define( 'MANAGERPATH', BASEPATH . '/manager' );

define('SERVERTIME', time());